import Member from "../mongodb/models/member.js";
import * as dotenv from "dotenv";
import { v2 as cloudinary } from "cloudinary";
import mongoose from "mongoose";

dotenv.config();

cloudinary.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
});

const getAllMembers = async (req, res) => {
  try {
    const members = await Member.find();
    res.status(200).json(members);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
const createMember = async (req, res) => {
  try {
    const {
      address,
      firstName,
      birthday,
      contactNumber,
      educationalAttainment,
      email,
      emergencyContactNumber,
      emergencyContactPerson,
      faceToFace,
      followUp1,
      followUp2,
      followUp3,
      followUp4,
      followUp5,
      followUp6,
      gender,
      gifts_administration,
      gifts_encouragement,
      gifts_evangelism,
      gifts_faith,
      gifts_giving,
      gifts_healing,
      gifts_hospitality,
      gifts_knowledge,
      gifts_leadership,
      gifts_mercy,
      gifts_miracles,
      gifts_prophecy,
      lastName,
      lifeGear101,
      lifeGear201,
      lifeGear301,
      lifeGear401,
      lifeGear501,
      middleName,
      missionExposure_community,
      missionExposure_foreign,
      missionExposure_none,
      missionExposure_other,
      missionExposure_provincial,
      missionaryPartner,
      nameOfDisciple,
      nameOfDiscipler,
      omega2,
      omega4,
      preferredLannguage,
      primaryMinistry,
      sm_creatives,
      sm_kids,
      sm_mission,
      sm_northCemetery,
      sm_praiseAndWorship,
      sm_prayer,
      sm_prison,
      sm_school,
      sm_ushering,
      sm_youth,
      suffix,
      trainings_doctrinalClass2023,
      trainings_evangelism,
      trainings_lifeOnMission,
      trainings_multiMediaClass,
      trainings_phileoEmpowering,
      trainings_teachers,
      trainings_usheringTeam,
      trainings_worshipTeam,
      virtual,
      memberType,
      isActive,
      waterBaptism,
      photo,
    } = req.body;

    // filter falsy values

    const followUpArr = [
      followUp1,
      followUp2,
      followUp3,
      followUp4,
      followUp5,
      followUp6,
    ];

    const lifeGearArr = [
      lifeGear101,
      lifeGear201,
      lifeGear301,
      lifeGear401,
      lifeGear501,
    ];

    const missionExposureArr = [
      missionExposure_community,
      missionExposure_foreign,
      missionExposure_none,
      missionExposure_other,
      missionExposure_provincial,
    ];

    const giftsArr = [
      gifts_administration,
      gifts_encouragement,
      gifts_evangelism,
      gifts_faith,
      gifts_giving,
      gifts_healing,
      gifts_hospitality,
      gifts_knowledge,
      gifts_leadership,
      gifts_mercy,
      gifts_miracles,
      gifts_prophecy,
    ];

    const secondaryMinistryArr = [
      sm_creatives,
      sm_kids,
      sm_mission,
      sm_northCemetery,
      sm_praiseAndWorship,
      sm_prayer,
      sm_prison,
      sm_school,
      sm_ushering,
      sm_youth,
    ];

    const trainingsArr = [
      trainings_doctrinalClass2023,
      trainings_evangelism,
      trainings_lifeOnMission,
      trainings_multiMediaClass,
      trainings_phileoEmpowering,
      trainings_teachers,
      trainings_usheringTeam,
      trainings_worshipTeam,
    ];

    const omegaArr = [omega2, omega4];
    const serviceTypeArr = [faceToFace, virtual];

    const filteredFollowupArr = followUpArr.filter((item) => item);
    const filteredGiftsArr = giftsArr.filter((item) => item);
    const filteredLifeGearArr = lifeGearArr.filter((item) => item);
    const filteredMissionExposureArr = missionExposureArr.filter(
      (item) => item
    );
    const filteredOmegaArr = omegaArr.filter((item) => item);
    const filteredSecondaryMinistryArr = secondaryMinistryArr.filter(
      (item) => item
    );
    const filteredTrainingsArr = trainingsArr.filter((item) => item);
    const filteredServiceTypeArr = serviceTypeArr.filter((item) => item);

    const photoUrl = await cloudinary.uploader.upload(photo);

    const newMember = await Member.create({
      profile: {
        firstName,
        middleName,
        lastName,
        suffix,
        gender,
        birthday,
        contactNumber,
        email,
        address,
        emergencyContactNumber,
        emergencyContactPerson,
        educationalAttainment,
        preferredLannguage,
        memberType,
        photo: photoUrl.url,
        isActive,
      },
      membership: {
        followUpSeries: filteredFollowupArr,
        lifeGearSeries: filteredLifeGearArr,
        omegaSeries: filteredOmegaArr,
        serviceType: filteredServiceTypeArr,
        waterBaptism,
      },
      maturity: {
        trainigns: filteredTrainingsArr,
        discipler: nameOfDiscipler,
        disciples: nameOfDisciple,
      },
      ministry: {
        primary: primaryMinistry,
        secondary: filteredSecondaryMinistryArr,
        spiritualGifts: filteredGiftsArr,
      },
      mission: {
        missionaryPartner,
        missionExposure: filteredMissionExposureArr,
      },
    });

    res.status(200).json(newMember);
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};
const getMemberInfoByID = async (req, res) => {
  try {
    const { id } = req.params;
    const member = await Member.findOne({ _id: id });
    if (member) {
      console.log(member);
      const { profile, membership, mission, maturity, ministry } = member;

      const {
        address,
        firstName,
        birthday,
        contactNumber,
        educationalAttainment,
        email,
        gender,
        lastName,
        middleName,
        preferredLannguage,
        suffix,
        memberType,
        isActive,
        photo,
        emergencyContactNumber,
        emergencyContactPerson,
      } = profile;

      const {
        followUpSeries,
        lifeGearSeries,
        omegaSeries,
        serviceType,
        waterBaptism,
      } = membership;

      const memberInfo = {
        address,
        firstName,
        birthday,
        contactNumber,
        educationalAttainment,
        email,
        gender,
        lastName,
        middleName,
        preferredLannguage,
        suffix,
        memberType,
        isActive,
        photo,
        emergencyContactNumber,
        emergencyContactPerson,
        lifeGear101: lifeGearSeries.includes("Life Gear 101") ? true : false,
        lifeGear201: lifeGearSeries.includes("Life Gear 201") ? true : false,
        lifeGear301: lifeGearSeries.includes("Life Gear 301") ? true : false,
        lifeGear401: lifeGearSeries.includes("Life Gear 401") ? true : false,
        lifeGear501: lifeGearSeries.includes("Life Gear 501") ? true : false,
      };

      console.log(memberInfo);

      res.status(200).json(memberInfo);
    } else {
      res.status(404).json({ message: "Member not found." });
    }
  } catch (error) {
    res.status(500).json({ message: error.message });
  }
};

export { getAllMembers, createMember, getMemberInfoByID };

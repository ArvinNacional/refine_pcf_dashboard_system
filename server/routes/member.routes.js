import express from "express";
import {
  createMember,
  getAllMembers,
  getMemberInfoByID,
} from "../controllers/member.controller.js";

const router = express.Router();

router.route("/").get(getAllMembers);
router.route("/:id").get(getMemberInfoByID);
router.route("/").post(createMember);

export default router;

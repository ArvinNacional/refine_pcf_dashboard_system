import mongoose from "mongoose";
const MemberSchema = new mongoose.Schema(
  {
    profile: {
      firstName: {
        type: String,
        required: true,
      },
      middleName: {
        type: String,
        default: "",
      },
      gender: {
        type: String,
      },
      lastName: {
        type: String,
        required: true,
      },
      suffix: {
        type: String,
        default: "",
      },
      birthday: {
        type: Date,
      },
      contactNumber: {
        type: Number,
      },
      email: {
        type: String,
      },
      address: {
        type: String,
      },
      emergencyContactPerson: {
        type: String,
      },
      emergencyContactNumber: {
        type: String,
      },
      educationalAttainment: {
        type: String,
      },
      preferredLanguage: {
        type: String,
      },
      memberType: {
        type: String,
        default: "member",
      },
      photo: {
        type: String,
        default: "memberPhoto",
      },
      isActive: {
        type: Boolean,
      },
    },

    membership: {
      followUpSeries: [
        {
          type: String,
        },
      ],

      lifeGearSeries: [{ type: String }],
      omegaSeries: [{ type: String }],

      serviceType: [{ type: String }],
      waterBaptism: {
        type: String,
      },
    },
    maturity: {
      trainings: [{ type: String }],
      discipler: { type: String },
      disciples: { type: String },
    },
    ministry: {
      primary: {
        type: String,
      },
      secondary: [{ type: String }],
      spiritualGifts: [{ type: String }],
      heart: [{ type: String }],
      abilities: [
        {
          type: String,
        },
      ],
      personality: [
        {
          type: String,
        },
      ],
      educationalExperience: [{ type: String }],
      workExperience: [{ type: String }],
    },
    mission: {
      missionExposure: [{ type: String }],
      missionaryPartner: {
        type: Boolean,
        default: false,
      },
    },
  },
  {
    timestamps: true,
  }
);

const memberModel = mongoose.model("Member", MemberSchema);
export default memberModel;
